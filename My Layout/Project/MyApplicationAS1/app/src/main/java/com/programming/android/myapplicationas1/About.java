package com.programming.android.myapplicationas1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void back (View view){
        Button btnback = findViewById(R.id.btnback);
        Intent intent = new Intent(About.this,MainActivity2.class);
        startActivity(intent);
    }

    public void nextweb (View view){
        Button nextweb = findViewById(R.id.btnback);
        Intent intent = new Intent(About.this,MainActivityWeb.class);
        startActivity(intent);
    }
}